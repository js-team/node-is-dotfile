# Installation
> `npm install --save @types/is-dotfile`

# Summary
This package contains type definitions for is-dotfile (https://github.com/jonschlinkert/is-dotfile).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/is-dotfile

Additional Details
 * Last updated: Tue, 25 Dec 2018 05:19:05 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by BendingBender <https://github.com/BendingBender>.
